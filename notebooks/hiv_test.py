# %%
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LogisticRegression
from hiv_dataset_generator import create_dataset, plot_data_distribution

# %%
# A billion dataset would generate extreme values in the distribution center, hard to converge for a linear model
hiv_dataset = create_dataset(1000)
# Calculate train/test length
if hiv_dataset.shape[0] >= 10**6:
    test_length = 20000
else:
    test_length = round(hiv_dataset.shape[0] * 0.3)
train_length = hiv_dataset.shape[0] - test_length
# Slice sets
train_set = hiv_dataset[:train_length,]
test_set = hiv_dataset[-test_length:,]

# %%
plot_data_distribution(test_set[:,0:1])

# %%
print(train_set[:,0:1].shape) # X is 2D array with one column
print(train_set[:,1].shape) # y is 1D array
classifier = LogisticRegression().fit(train_set[:,0:1], train_set[:,1])

# %%
classifier.score(test_set[:,0:1], test_set[:,1])

# %%
example_number = 0
example_white_cells = test_set[example_number:example_number+1,0:1]
example_hiv = test_set[example_number,1]
example_hiv_prediction = classifier.predict(example_white_cells)
print(f'white cells per liter --> {example_white_cells[0,0]}')
print(f'Hiv? --> {example_hiv}')
print(f'Hiv prediction --> {example_hiv_prediction[0]}')
