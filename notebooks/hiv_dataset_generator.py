import numpy as np
import matplotlib.pyplot as plt


def _create_features(up_to, n_samples):
    dim = (n_samples, 1)
    white_cells = np.sort(np.random.binomial(n=up_to, p=0.1, size=n_samples), axis=0)
    white_cells = np.array([white_cells]).T
    return white_cells


def _create_targets(samples):
    n_samples = samples.shape[0]
    n_negatives = n_samples//2
    n_positives = n_samples - n_negatives
    hiv_results = np.concatenate((np.zeros((n_negatives)), np.ones((n_positives))))
    hiv_results = np.array([hiv_results]).T
    return hiv_results


def create_dataset(up_to=10**9, n_samples=10**6, shuffle=True):
    white_cells = _create_features(up_to, n_samples)
    hiv_results = _create_targets(white_cells)
    hiv_dataset = np.concatenate((white_cells, hiv_results), axis=1)
    if shuffle:
        np.random.shuffle(hiv_dataset)
    return hiv_dataset


def plot_data_distribution(data):
    plt.hist(data[:,0], bins='auto')
    plt.show()
